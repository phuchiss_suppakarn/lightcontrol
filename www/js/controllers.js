'use strict';

/* Controllers */
function HomeCtrl($scope, navSvc, $rootScope) {
    $rootScope.showSettings = false;
    $scope.slidePage = function (path, type) {
        navSvc.slidePage(path, type);
    };
    $scope.slideDevice = function (path, type, device) {
        if (device) {
            $rootScope.deviceno = device;
        }
        navSvc.slidePage(path, type);
    };
    $scope.back = function () {
        navSvc.back();
    };
    $scope.changeSettings = function () {
        $rootScope.showSettings = true;
    };
    $scope.closeOverlay = function () {
        $rootScope.showSettings = false;
    };
}

function SettingCtrl($scope, $http, $rootScope, navSvc) {
    $scope.settingDefault =
    {   url: "http://build-uptech.com",
        password: "123456",
        labelDevice: "Device 1",
        labelSwitch1: "Switch 1",
        labelSwitch2: "Switch 2",
        labelSwitch3: "Switch 3"   };

    $scope.setting = $scope.settingDefault;

    if ($rootScope.deviceno) {
        $scope.setting = JSON.parse(window.localStorage.getItem($rootScope.deviceno));

        log("Get " + $rootScope.deviceno + " : " + JSON.stringify($scope.setting));
        log("URL : " + $scope.setting.url);
    }

    $scope.submit = function () {
        if (this.deviceno) {
            log("Device Number " + this.deviceno);
        }

        window.localStorage.setItem($rootScope.deviceno, JSON.stringify(this.setting));
        log("Save Setting : " + $rootScope.deviceno + " : " + JSON.stringify(this.setting))

        navSvc.slidePage('/view1', 'modal');
    };

    var s1 = window.localStorage.getItem("Setting1")
    if (s1) {
        $scope.setting1 = JSON.parse(window.localStorage.getItem("Setting1"))
    }else{
        window.localStorage.setItem("Setting1", JSON.stringify($scope.settingDefault));
        $scope.setting1 =  $scope.settingDefault;
    }
    log(JSON.stringify($scope.setting1))

    var s2 = window.localStorage.getItem("Setting2")
    if (s2) {
        $scope.setting2 = JSON.parse(window.localStorage.getItem("Setting2"))
    }else{
        window.localStorage.setItem("Setting2", JSON.stringify($scope.settingDefault));
        $scope.setting2 =  $scope.settingDefault;
    }
    log(JSON.stringify($scope.setting2))

    var s3 = window.localStorage.getItem("Setting3")
    if (s3) {
        $scope.setting3 = JSON.parse(window.localStorage.getItem("Setting3"))
    }else{
        window.localStorage.setItem("Setting3", JSON.stringify($scope.settingDefault));
        $scope.setting3 =  $scope.settingDefault;
    }
    log(JSON.stringify($scope.setting3))

    var s4 = window.localStorage.getItem("Setting4")
    if (s4) {
        $scope.setting4 = JSON.parse(window.localStorage.getItem("Setting4"))
    }else{
        window.localStorage.setItem("Setting4", JSON.stringify($scope.settingDefault));
        $scope.setting4 =  $scope.settingDefault;
    }
    log(JSON.stringify($scope.setting4))

    var s5 = window.localStorage.getItem("Setting5")
    if (s5) {
        $scope.setting5 = JSON.parse(window.localStorage.getItem("Setting5"))
    }else{
        window.localStorage.setItem("Setting5", JSON.stringify($scope.settingDefault));
        $scope.setting5 =  $scope.settingDefault;
    }
    log(JSON.stringify($scope.setting5))
}

function LightCtrl($scope, $http, $rootScope, $timeout) {
    $scope.ch1 = "OFF";
    $scope.ch2 = "OFF";
    $scope.ch3 = "OFF";
    $scope.status = "OFFLINE";
    $scope.lastSync = null

    var setting = window.localStorage.getItem($rootScope.deviceno);

    $scope.setting =
    {   url: "http://build-uptech.com",
        password: "123456",
        labelDevice: "Device 1",
        labelSwitch1: "Switch 1",
        labelSwitch2: "Switch 2",
        labelSwitch3: "Switch 3"   };

    if (setting) {
        log("LigthCtrl have setting : " + $rootScope.deviceno)
        $scope.setting = JSON.parse(window.localStorage.getItem($rootScope.deviceno));
    } else {
        log("LigthCtrl exist setting : " + $rootScope.deviceno)
        window.localStorage.setItem($rootScope.deviceno, JSON.stringify($scope.setting));
    }

    $http({method: 'GET', url: $scope.setting.url + "/" + $scope.setting.password + "/"}).
        success(function (data, status) {
            log(data)
            $scope.ch1 = $(data).find('#ch1').text().trim()
            $scope.ch2 = $(data).find('#ch2').text().trim()
            $scope.ch3 = $(data).find('#ch3').text().trim()

            $scope.status = "ONLINE"
            $scope.lastSync = new Date().toString();
        }).
        error(function (data, status) {
            log("Rest fail statuscode : " + status);
            $scope.status = "OFFLINE"
            $scope.lastSync = new Date().toString();
        })

    $scope.style = function (value) {
        if (value == "OFFLINE") {
            return { "color": "red" };
        } else {
            return { "color": "green" };
        }
    }

    $scope.styleSwitch = function (value) {
        if (value == "OFF") {
            return { "background-color": "red" };
        } else {
            return { "background-color": "green" };
        }
    }

    $scope.ch1Click = function () {
        if ($scope.ch1 == "OFF") {
            $scope.ch1 = "ON";
        } else {
            $scope.ch1 = "OFF";
        }
        log("Request URL : " + $scope.setting.url + "/" + $scope.setting.password + "/CH1")
        $http({method: 'GET', url: $scope.setting.url + "/" + $scope.setting.password + "/CH1"}).
            success(function (data, status) {
                log(data)

                $scope.ch1 = $(data).find('#ch1').text().trim()
                $scope.ch2 = $(data).find('#ch2').text().trim()
                $scope.ch3 = $(data).find('#ch3').text().trim()

                $scope.status = "ONLINE"
                $scope.lastSync = new Date().toString();
            }).
            error(function (data, status) {
                log("Rest fail statuscode : " + status);
                $scope.status = "OFFLINE"
                $scope.lastSync = new Date().toString();
            })
    }

    $scope.ch2Click = function () {
        if ($scope.ch2 == "OFF") {
            $scope.ch2 = "ON";
        } else {
            $scope.ch2 = "OFF";
        }
        log("Request URL : " + $scope.setting.url + "/" + $scope.setting.password + "/CH2")
        $http({method: 'GET', url: $scope.setting.url + "/" + $scope.setting.password + "/CH2"}).
            success(function (data, status) {
                log(data)

                $scope.ch1 = $(data).find('#ch1').text().trim()
                $scope.ch2 = $(data).find('#ch2').text().trim()
                $scope.ch3 = $(data).find('#ch3').text().trim()

                $scope.status = "ONLINE"
                $scope.lastSync = new Date().toString();
            }).
            error(function (data, status) {
                log("Rest fail statuscode : " + status);
                $scope.status = "OFFLINE"
                $scope.lastSync = new Date().toString();
            })
    }

    $scope.ch3Click = function () {
        if ($scope.ch3 == "OFF") {
            $scope.ch3 = "ON";
        } else {
            $scope.ch3 = "OFF";
        }
        log("Request URL : " + $scope.setting.url + "/" + $scope.setting.password + "/CH3")
        $http({method: 'GET', url: $scope.setting.url + "/" + $scope.setting.password + "/CH3"}).
            success(function (data, status) {
                log(data)

                $scope.ch1 = $(data).find('#ch1').text().trim()
                $scope.ch2 = $(data).find('#ch2').text().trim()
                $scope.ch3 = $(data).find('#ch3').text().trim()

                $scope.status = "ONLINE"
                $scope.lastSync = new Date().toString();
            }).
            error(function (data, status) {
                log("Rest fail statuscode : " + status);
                $scope.status = "OFFLINE"
                $scope.lastSync = new Date().toString();
            })
    }

    $scope.testSwitch = function (value) {
        if (value == "ch1") {
            if ($scope.ch1 == "OFF") {
                $scope.ch1 = "ON";
            } else {
                $scope.ch1 = "OFF";
            }
        } else if (value == "ch2") {
            if ($scope.ch2 == "OFF") {
                $scope.ch2 = "ON";
            } else {
                $scope.ch2 = "OFF";
            }
        } else if (value == "ch3") {
            if ($scope.ch3 == "OFF") {
                $scope.ch3 = "ON";
            } else {
                $scope.ch3 = "OFF";
            }
        } else {
            $http({method: 'GET', url: 'http://192.168.1.2/test.html', cache: $templateCache}).
                success(function (data, status) {
                    log(data)

                    $scope.ch1 = $(data).find('#ch1').text()
                    $scope.ch2 = $(data).find('#ch2').text()
                    $scope.ch3 = $(data).find('#ch3').text()

                    $scope.status = "ONLINE"
                }).
                error(function (data, status) {
                    log("Rest fail statuscode : " + status);
                    $scope.status = "OFFLINE"
                })
        }
    }



    $scope.onTimeout = function(){
        log("Request URL from timeout : " + $scope.setting.url + "/" + $scope.setting.password + "/")
        $http({method: 'GET', url: $scope.setting.url + "/" + $scope.setting.password + "/"}).
            success(function (data, status) {
                log(data)
                $scope.ch1 = $(data).find('#ch1').text().trim()
                $scope.ch2 = $(data).find('#ch2').text().trim()
                $scope.ch3 = $(data).find('#ch3').text().trim()

                $scope.status = "ONLINE"
                $scope.lastSync = new Date().toString();
                mytimeout = $timeout($scope.onTimeout,5000);
            }).
            error(function (data, status) {
                log("Rest fail statuscode : " + status);
                $scope.status = "OFFLINE"
                $scope.lastSync = new Date().toString();
                mytimeout = $timeout($scope.onTimeout,5000);
            })
    }
    var mytimeout = $timeout($scope.onTimeout,5000);
}

function log() {
    console.log.apply(console, arguments)
}



                     